## Описание

Две программы, позволяющие передавать комманды/данные от одной плате к другой. Файл "pub" используется
для публикации данных, "sub" для принятия.
