const int buttonPin = 2;
const int ledPin = 9;
const int latchPin = 5;
const int clockPin = 3;
const int dataPin = 7;

volatile bool buttonState = LOW;
volatile bool buttonPressed = false;
volatile unsigned long lastDebounceTime = 0;
volatile unsigned long debounceDelay = 200;

volatile bool ledOn = false;
volatile int brightness = 0;
volatile int brightnessIncrement = 5;

unsigned long lastBrightnessTime = 0;
const unsigned long brightnessInterval = 50;

unsigned long lastDigitTime = 0;
const int digitInterval = 3000; // Интервал между цифрами в миллисекундах
int currentDigit = 0;

bool digits[10][8] = {
  {1, 1, 0, 1, 1, 1, 0, 1},  // 0
  {0, 1, 0, 1, 0, 0, 0, 0},  // 1
  {1, 1, 0, 0, 1, 1, 1, 0},  // 2
  {1, 1, 0, 1, 1, 0, 1, 0},  // 3
  {0, 1, 0, 1, 0, 0, 1, 1},  // 4
  {1, 0, 0, 1, 1, 0, 1, 1},  // 5
  {1, 0, 1, 1, 1, 1, 1, 1},  // 6
  {1, 1, 0, 1, 0, 0, 0, 0},  // 7
  {1, 1, 0, 1, 1, 1, 1, 1},  // 8
  {1, 1, 1, 1, 1, 0, 1, 1}   // 9
};

void setup() {
  pinMode(buttonPin, INPUT_PULLUP);
  pinMode(ledPin, OUTPUT);
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);

  attachInterrupt(digitalPinToInterrupt(buttonPin), buttonDelay, CHANGE);
  Serial.begin(9800);
}

void loop() {
  if (buttonPressed) {
    buttonPressed = false;

    if (!ledOn) {
      ledOn = true;
      analogWrite(ledPin, brightness);
      currentDigit = 0;
    } else {
      while (digitalRead(buttonPin) == LOW) {
        adjustBrightness();
        updateDigit();
      }
      ledOn = false;
      analogWrite(ledPin, 0);
      showNumber(0);
    }
  }
}

void buttonDelay() {
  if (millis() - lastDebounceTime > debounceDelay) {
    buttonState = digitalRead(buttonPin);

    if (buttonState == LOW) {
      buttonPressed = true;
      Serial.println("Button Pressed");
    } else {
      Serial.println("Button Released");
    }

    lastDebounceTime = millis();
  }
}

void adjustBrightness() {
  if (ledOn) {
    if (millis() - lastBrightnessTime >= brightnessInterval) {
      lastBrightnessTime = millis();
      brightness += brightnessIncrement;
      if (brightness <= 0 || brightness >= 255) {
        brightnessIncrement *= -1;
      }
      analogWrite(ledPin, brightness);
    }
  }
}


void updateDigit() {
  if (millis() - lastDigitTime >= digitInterval) {
    lastDigitTime = millis();
    showNumber(currentDigit);
    currentDigit = (currentDigit + 1) % 10;
  }
}

void showNumber(int digit) {
  digitalWrite(latchPin, LOW);
  if (digit < 0 || digit > 9) {
    return;
  }
  for (int i = 7; i >= 0; i--) {
    shiftAndSet(digits[digit][i]);
  }
  digitalWrite(latchPin, HIGH);
}


void shiftAndSet(int value) {
  digitalWrite(clockPin, LOW);
  digitalWrite(dataPin, value);
  digitalWrite(clockPin, HIGH);
}





