## Описание
Данная работа представляет собой схему, состоящая из следующих компонентов - плата Arduino Uno, светодиода, кнопки, 7-сегментного индикатора и сдвигового регистра.

При простом нажатии кнопки, светодиод включается/выключается, при зажатии - меняется яркость светодиода, а также начинается отсчёт времени зажатия на 7-сегментном индикаторе. Для устранения "дребезга" кнопки был использован attachInterrupt и millis().

![Схема в tinkercad](scheme.png) 
![Реальный прототип](real.png)
