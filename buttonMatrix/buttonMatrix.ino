const int numRows = 2;
const int numCols = 2;

int rows[numRows] = {2, 3};
int cols[numCols] = {4, 5};

void setup() {
    for (int i = 0; i < numCols; i++) {
        pinMode(cols[i], OUTPUT);
    }
    for (int i = 0; i < numRows; i++) {
        pinMode(rows[i], OUTPUT);
    }
    for (int i = 0; i < numCols; i++) {
        pinMode(cols[i], INPUT_PULLUP);
    }
    Serial.begin(9600);
}


void loop() {
for (int i = 0; i < numRows; i++) {
digitalWrite(rows[i], HIGH);
for (int j = 0; j < numCols; j++) {
int buttonState = digitalRead(cols[j]);
Serial.print("Row: ");
Serial.print(i + 1);
Serial.print(", Col: ");
Serial.print(j + 1);
Serial.print(", State: ");
Serial.println(buttonState);
delay(1000);
}
digitalWrite(rows[i], LOW);
}
Serial.println("-------------------------");
}