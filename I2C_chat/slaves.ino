#include <Wire.h>

String receivedMessage;
int messageLength;
String userInputToSend;
bool next = false;
String nameOfRecipient = "";
String senderName = "sava";

void processUserInput(String recipientName) {
  if (Serial.available() > 0) {
    String userInput = Serial.readStringUntil('\n');
    int colonIndex = userInput.indexOf(':');
    if (colonIndex != -1) {
      String recipientName = userInput.substring(0, colonIndex);
      String message = userInput.substring(colonIndex + 1);
      userInputToSend = recipientName + ":" + message + "|" + senderName;
      Serial.println("Message sent to " + recipientName + ":" + message);
    } else {
      userInputToSend = recipientName + ":" + userInput + "|" + senderName;
      nameOfRecipient = "";
      Serial.println("you replied to " + recipientName + ":" + userInput);
    }
    messageLength = userInputToSend.length();
  }
}

void respondToMessage(String message) {
  int colonIndex = message.indexOf(':');
  if (colonIndex != -1) {
    String senderName = message.substring(0, colonIndex);
    String receivedMessage = message.substring(colonIndex + 1);
    Serial.println("Give to " + senderName + " an answer");
    nameOfRecipient = senderName;
  }
}

void setup() {
  Wire.begin(2);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestEvent);
  Serial.begin(9600);
}

void loop() {
  processUserInput(nameOfRecipient);
}

void receiveEvent(int length) {
  receivedMessage = "";
  while (Wire.available()) {
    char c = Wire.read();
    receivedMessage += c;
  }
  Serial.println(receivedMessage);
  respondToMessage(receivedMessage);
}

void requestEvent() {
  if (messageLength > 0) {
    if (next) {
      Wire.write(userInputToSend.c_str());
      userInputToSend = "";
      next = false;
      messageLength = 0;
    } else {
      Wire.write(messageLength);
      next = true;
    }
  }
}
