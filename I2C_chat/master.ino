#include <Wire.h>

const char *boardNames[][2] = {
  { "2", "sava" },
  { "3", "tevsae" },
  { "4", "gojo" }
};

void sendMessage(const char *recipientName, const char *message, const char *senderName) {
  for (auto &board : boardNames) {
    if (strcmp(board[1], recipientName) == 0) {
      Wire.beginTransmission(atoi(board[0]));
      Wire.write(senderName);
      Wire.write(":");
      Wire.write(message);
      Wire.endTransmission();
      break;
    }
  }
}

void processUserInput(String msg, int length) {
  if (length > 0) {
    int colonIndex = msg.indexOf(':');
    if (colonIndex != -1) {
      String recipientName = msg.substring(0, colonIndex);
      int pipeIndex = msg.indexOf('|', colonIndex + 1);
      if (pipeIndex != -1) {
        String message = msg.substring(colonIndex + 1, pipeIndex);
        String senderName = msg.substring(pipeIndex + 1);
        sendMessage(recipientName.c_str(), message.c_str(), senderName.c_str());
      }
    }
  }
}

void setup() {
  Serial.begin(9600);
  Wire.begin();
}

void loop() {
  for (auto &board : boardNames) {
    int messageLength = 0;
    String msg;
    Wire.requestFrom(atoi(board[0]), 1);
    while (Wire.available()) {
      char c = Wire.read();
      messageLength = (int) c;
    }
    if (messageLength > 0) {
      Wire.requestFrom(atoi(board[0]), messageLength);
      while (Wire.available()) {
        char c = Wire.read();
        msg.concat(c);
      }
      Serial.print("got message to send - ");
      Serial.println(msg);
      processUserInput(msg, messageLength);
      Serial.println();
      delay(100);
    }
  }

  delay(1000);
}
