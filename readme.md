# Internet Of Things



## Описание
Освоение теоретических и практических знаний, навыков в области разработки электронных устройств на базе микроконтроллеров и программных средств для решения задач в сфере "Интернета вещей".

## Description
Learning of theoretical and practical knowledge, skills in the field of development of electronic devices based on microcontrollers and software tools to solve problems in the field of "Internet of Things".