#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>

ESP8266WebServer server(WEB_SERVER_PORT);

const char* pingHost = "www.yandex.ru";

void handle_root() {
  String page_code = "<form action=\"/PROCESS\" method=\"POST\">";
  page_code += "Client name:<input type=\"text\" name=\"cli_ssid\" placeholder=\"Input wifi name\"><br>";
  page_code += "Password:<input type=\"password\" name=\"cli_pass\" placeholder=\"Input wifi name\"><br>";
  page_code += "<input type=\"submit\" value=\"Send\"></form>";
  server.send(200, "text/html", page_code);
}

void handle_process() {
  String message = "Number of args received: " + String(server.args()) + "\n";

  for (int i = 0; i < server.args(); i++) {
    message += "Arg nº" + String(i) + " --> ";
    message += server.argName(i) + ": ";
    message += server.arg(i) + "\n";
  }
  if (AP_mode) {     
    AP_mode = false;     // Попытка подключения к Wi-Fi в режиме клиента
    start_client_mode();  // Отключить точку доступа
    server.send(200, "text/plain", "Wi-Fi connected, Access Point turned off.");
  } else {
    AP_mode = true;
    start_AP_mode();  // Если подключение не удалось, включить точку доступа
    server.send(200, "text/plain", "Wi-Fi connection failed, Access Point is active.");
  }
  server.send(303);
}

void handle_led() {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  server.sendHeader("Location", "/");
  server.send(303);
}

void handle_not_found() {
  server.send(404, "text/html", "404: check URL");
}

void handle_sensor() {
  int val = analogRead(A0);
  server.send(200, "text/html", String(val));
}

void server_init() {
  server.on("/", HTTP_GET, handle_root);
  server.on("/PROCESS", HTTP_POST, handle_process);
  server.onNotFound(handle_not_found);

  server.begin();
  Serial.print("Server started on ");
  Serial.println(WEB_SERVER_PORT);
}