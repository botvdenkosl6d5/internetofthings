#include "config.h"
#include "wifi.h"
#include "server.h"
#include "MQTT.h"

String commandTopic = "esp8266-ssss/command";
String stateTopic = "esp8266-ssss/state";

bool is_wifi_on = false;
unsigned long lastCheckTime = 0;
const unsigned long checkInterval = 5000;
bool internetAvailable;

bool isInternetAvailable() {
  WiFiClient client;
  return client.connect("www.yandex.ru", 80);
}

void setup() {
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);
  is_wifi_on = init_WIFI(AP_mode);
  if (is_wifi_on) {
    server_init();
  }
}

void loop() {
  server.handleClient();
  
  unsigned long currentTime = millis();
  if (currentTime - lastCheckTime >= checkInterval) {
    lastCheckTime = currentTime;
    bool internetAvailable = isInternetAvailable();

    if (!AP_mode && internetAvailable) {
      // Если устройство в режиме клиента и есть интернет, продолжаем работу с MQTT
      if (!mqtt_client.connected()) {
        // Если MQTT не подключен, инициализируем MQTT
        is_wifi_on = init_MQTT(commandTopic, stateTopic);
      }
    } else {
      // Если устройство в режиме клиента, но нет интернета, отключаем MQTT
      if (mqtt_client.connected()) {
        mqtt_client.disconnect();
        Serial.println("Disconnected from MQTT");
      }
    }

    // Если устройство в режиме точки доступа и есть интернет, переключаем в режим клиента
    if (AP_mode && internetAvailable) {
      AP_mode = false;
      is_wifi_on = init_WIFI(AP_mode);
      if (is_wifi_on) {
        server_init();
      }
    }

    // Если устройство в режиме клиента, но нет интернета, переключаем в режим точки доступа
    if (!AP_mode && !internetAvailable) {
      AP_mode = true;
      is_wifi_on = init_WIFI(AP_mode);
      if (is_wifi_on) {
        server_init();
      }
    }
  }

  if (!AP_mode) {
    mqtt_client.loop();
  }
  delay(20);
}

