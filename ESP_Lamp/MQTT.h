#include <PubSubClient.h>

PubSubClient mqtt_client(wifiClient);

void callback(char *topic, byte *payload, unsigned int length) {
  Serial.print("There is a message on topic: ");
  Serial.println(topic);
  Serial.print("Message is: ");
  char messageChar = (char)payload[0];
  Serial.println(messageChar);

  if (messageChar == 'u') {
    digitalWrite(led_pin, LOW); // Включить светодиод
    Serial.println("LED on");
  } else if (messageChar == 'd') {
    digitalWrite(led_pin, HIGH); // Выключить светодиод
    Serial.println("LED off");
  }
}

void publishState(const String &stateTopic, const String &state) {
  mqtt_client.publish(stateTopic.c_str(), state.c_str());
}

bool init_MQTT(const String &commandTopic, const String &stateTopic) {
  mqtt_client.setServer(mqtt_broker, mqtt_port);
  mqtt_client.setCallback(callback);
  while (!mqtt_client.connected()) {
    String client_id = "esp8266_" + id();
    if (mqtt_client.connect(client_id.c_str())) {
      Serial.println("MQTT client connected with id " + client_id);
      mqtt_client.subscribe(commandTopic.c_str());
      mqtt_client.publish(stateTopic.c_str(), "hello");
      Serial.println("See me at " + stateTopic);
    } else {
      Serial.println("Failed to connect MQTT client with id " + client_id);
      delay(500);
    }
  }
  return true;
}
