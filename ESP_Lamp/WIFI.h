#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti wifiMulti;
WiFiClient wifiClient;

String ip = "IP not set";
bool internetConnected = false;

String id() {
  int mac_len = WL_MAC_ADDR_LENGTH;
  uint8_t mac[mac_len];
  WiFi.softAPmacAddress(mac);
  String mac_id = String(mac[mac_len-2], HEX) + String(mac[mac_len-1], HEX);
  return mac_id;
}

bool start_AP_mode() {
  String ssid_id = AP_NAME;
  IPAddress ap_IP(192, 168, 4, 1);
  WiFi.disconnect();
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(ap_IP, ap_IP, IPAddress(255, 255, 255, 0));
  if(WiFi.softAP((AP_NAME + id()).c_str(), AP_PASSWORD.c_str())) {
    Serial.println("\nWiFi started in AP mode " + ssid_id);
    return true;
  } else {
    Serial.println("Failed to start AP mode.");
    return false;
  }
}


bool start_client_mode() {
  WiFi.softAPdisconnect(true);
  wifiMulti.addAP(CLI_SSID, CLI_PASS);
  if(wifiMulti.run() == WL_CONNECTED) {
    Serial.println("WiFi connected in client mode.");
    return true;
  } else {
    Serial.println("Failed to connect to WiFi in client mode.");
    return false;
  }
}

bool init_WIFI(bool AP_mode) {
  if (AP_mode) {
    return start_AP_mode();
  } else {
    return start_client_mode();
  }
}
