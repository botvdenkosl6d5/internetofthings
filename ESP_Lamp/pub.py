import time
import random
import paho.mqtt.client as mqtt

broker_address = "broker.emqx.io"
min_brightness_time_limit = 30

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print(f"Failed to connect, return code {rc}")

client = mqtt.Client(f'lab_{random.randint(10000, 99999)}')
client.on_connect = on_connect
client.connect(broker_address)

start_time = time.time()
brightness_time = 40

while True:
    elapsed_time = time.time() - start_time
    seconds = int(elapsed_time % 60) + 1
    print(f"Elapsed time: {seconds} seconds")

    if 20 <= seconds <= brightness_time:
        state = "u"
        print("Published state: u")
    else:
        state = "d"
        print("Published state: d")

    client.publish("esp8266-ssss/command", state)

    time.sleep(1)

    # Decrease brightness_time by 1 every minute
    if seconds % 60 == 0:
        brightness_time -= 1

        # Reset brightness_time to initial value if it reaches the minimum
        if brightness_time < min_brightness_time_limit:
            brightness_time = 40