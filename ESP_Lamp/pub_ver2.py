import time
import random
import paho.mqtt.client as mqtt

broker_address = "broker.emqx.io"
current_brightness_time_max = 40
current_brightness_time_min = 20
max_brightness_time_limit = 35
min_brightness_time_limit = 25

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to MQTT Broker!")
    else:
        print(f"Failed to connect, return code {rc}")

client = mqtt.Client(f'lab_{random.randint(10000, 99999)}')
client.on_connect = on_connect
client.connect(broker_address)

start_time = time.time()
brightness_time = 40

while True:
    elapsed_time = time.time() - start_time
    seconds = int(elapsed_time % 60) + 1
    print(f"Elapsed time: {seconds} seconds")

    if current_brightness_time_min <= seconds <= current_brightness_time_max:
        state = "u"
        print("Published state: u")
    else:
        state = "d"
        print("Published state: d")

    client.publish("esp8266-ssss/command", state)

    time.sleep(1)

    # Decrease brightness_time by 1 every minute
    if seconds % 60 == 0:
        current_brightness_time_max -= 1
        current_brightness_time_min += 1

        # Reset brightness_time to initial value if it reaches the minimum
        if current_brightness_time_max < max_brightness_time_limit and current_brightness_time_min > min_brightness_time_limit:
            current_brightness_time_max = 40
            current_brightness_time_min = 20
