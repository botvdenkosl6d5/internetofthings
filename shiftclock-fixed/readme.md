## Описание

Реализация цифровых часов (MM:SS) с помощью четырёх индикаторов и сдвиговых регистров.

[Видео демонстрация работы](https://drive.google.com/file/d/1_EV76bvMItjj7puJta7UbdOUhYficyUp/view?usp=drive_link)