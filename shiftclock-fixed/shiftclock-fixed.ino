// Latch pin (ST_CP) of 74HC595 to pin 5
int latchPin = 5;      
// Clock pin (SH_CP) of 74HC595 to pin 6
int clockPin = 3;
// Data pin (DS of 74HC595 to pin 6
int dataPin = 7;       
// Make sure MR is HIGH (connect to Vcc)

byte digits[10] = {
  0b11011101,  // 0
  0b01010000,  // 1
  0b11001110,  // 2
  0b11011010,  // 3
  0b01010011,  // 4
  0b10011011,  // 5
  0b10111111,  // 6
  0b11010000,  // 7
  0b11011111,  // 8
  0b11011011   // 9
};

unsigned long previousMillis = 0;
const long interval = 1000;  
int minutes = 0;
int seconds = 0;

//after this number, minutes part will be nullified
int minutesLimit = 99;

void setup() 
{
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);  
  pinMode(clockPin, OUTPUT);
  Serial.begin(9600);
}

void show_time(int minutes, int seconds) {
  
  // Display minutes
  updateShiftRegister(digits[minutes / 10]);
  updateShiftRegister(digits[minutes % 10]);
  
  // Display seconds
  updateShiftRegister(digits[seconds / 10]);
  updateShiftRegister(digits[seconds % 10]);

}

void loop() 
{
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis >= interval) {

    seconds++;  
    
    if (seconds >= 60) {
      seconds = 0;
      minutes++;

      if (minutes > minutesLimit) {
        minutes = 0;
      }
    }

    show_time(minutes, seconds);
    
    previousMillis = currentMillis;  
  }

  if (Serial.available() >= 4) {
    int inputTime = 0;

    for (int i = 0; i < 4; i++) {
      char digit = Serial.read();
      inputTime = inputTime * 10 + (digit - '0');
    }
    minutes = inputTime / 100;
    seconds = inputTime % 100;

    Serial.print("Set time - ");
    Serial.print(minutes);
    Serial.print(":");
    Serial.println(seconds);
  }
}
  
void updateShiftRegister(byte data) {
  shiftOut(dataPin, clockPin, LSBFIRST, data);
  digitalWrite(latchPin, HIGH);
  digitalWrite(latchPin, LOW);
}
