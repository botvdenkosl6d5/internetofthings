#define sensor_pin A0
#define sensor_led 3

bool streaming = true;
bool alarm_mode = false;
bool manual_led_state = false;
bool led_state;
long previous_send_time = 0;
long send_count = 0;
unsigned long previous_alarm_time = 0;
const unsigned long alarm_interval = 500; 

void setup()
{
  pinMode(sensor_led, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
  int val = analogRead(sensor_pin);
  long current_time = millis();
  data_reading();
  
  if (alarm_mode) {
    // Режим сигнализации
    if (current_time - previous_alarm_time >= alarm_interval) {
      previous_alarm_time = current_time;
      digitalWrite(sensor_led, !digitalRead(sensor_led));
    }
  } else {
    if (streaming && (current_time / 100 != send_count)) {
      send_photo_data();
      previous_send_time = current_time;
      send_count = current_time / 100;
      Serial.print("Time in millis: ");
      Serial.println(millis());
    }
  }
}

void send_photo_data() {
  int val = analogRead(sensor_pin);
  if (val <= 500){
    if (!manual_led_state || led_state == true) {
      manual_led_state = false;
      int brightness = map(val, 6, 679, 255, 0);
      analogWrite(sensor_led, brightness); 
    }
  }
  else{
    if (!manual_led_state || led_state == false) {
      manual_led_state = false;
      int brightness = map(val, 6, 679, 255, 0);
      analogWrite(sensor_led, brightness);
    }
  }
}

void data_reading() {
  if (Serial.available() > 0) {
    char command = Serial.read();
    if (command == 'o') {
      digitalWrite(sensor_led, HIGH);
      led_state = true;
      manual_led_state = true;
      Serial.println("Led enabled");
    } else if (command == 'f') {
      digitalWrite(sensor_led, LOW);
      led_state = false;
      manual_led_state = true;
      Serial.println("Led disabled");
    } else if (command == 'p') { 
      streaming = true;
    } else if (command == 's') { 
      send_photo_data();
    } else if (command == 'a') { 
      alarm_mode = true;
      Serial.println("Alarm mode enabled");
    } else if (command == 'b') { 
      alarm_mode = false;
      digitalWrite(sensor_led, LOW); 
      Serial.println("Alarm mode disabled");
    }
  }
}


